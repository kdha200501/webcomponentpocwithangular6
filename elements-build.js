const fs = require('fs-extra');
const concat = require('concat');

(async function build() {
  const files = [
    './dist/WebComponentPoc/runtime.js',
    './dist/WebComponentPoc/polyfills.js',
    './dist/WebComponentPoc/scripts.js',
    './dist/WebComponentPoc/main.js'
  ];

  await fs.ensureDir('elements');

  await concat(files, 'elements/web-component-poc.js');

})();
