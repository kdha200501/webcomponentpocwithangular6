import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WcNavComponent } from './wc-nav.component';

describe('WcNavComponent', () => {
  let component: WcNavComponent;
  let fixture: ComponentFixture<WcNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WcNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WcNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
