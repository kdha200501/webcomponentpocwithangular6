import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WcBodyComponent } from './wc-body.component';

describe('WcBodyComponent', () => {
  let component: WcBodyComponent;
  let fixture: ComponentFixture<WcBodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WcBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WcBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
