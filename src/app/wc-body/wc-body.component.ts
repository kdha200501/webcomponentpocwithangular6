import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'wc-body',
  templateUrl: './wc-body.component.html',
  styleUrls: ['./wc-body.component.scss'],
  encapsulation: ViewEncapsulation.Native
})
export class WcBodyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
