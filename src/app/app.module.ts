import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';

import { WcArticleComponent } from './wc-article/wc-article.component';
import { WcBodyComponent } from './wc-body/wc-body.component';
import { WcHeaderComponent } from './wc-header/wc-header.component';
import { WcNavComponent } from './wc-nav/wc-nav.component';

@NgModule({
  declarations: [
    WcArticleComponent,
    WcBodyComponent,
    WcHeaderComponent,
    WcNavComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  entryComponents: [
    WcArticleComponent,
    WcBodyComponent,
    WcHeaderComponent,
    WcNavComponent
  ]
})
export class AppModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap() {
    const wcArticleComponent = createCustomElement(WcArticleComponent, { injector: this.injector });
    customElements.define('wc-article', wcArticleComponent);

    const wcBodyComponent = createCustomElement(WcBodyComponent, { injector: this.injector });
    customElements.define('wc-body', wcBodyComponent);

    const wcHeaderComponent = createCustomElement(WcHeaderComponent, { injector: this.injector });
    customElements.define('wc-header', wcHeaderComponent);

    const wcNavComponent = createCustomElement(WcNavComponent, { injector: this.injector });
    customElements.define('wc-nav', wcNavComponent);
  }
}
