import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WcArticleComponent } from './wc-article.component';

describe('WcArticleComponent', () => {
  let component: WcArticleComponent;
  let fixture: ComponentFixture<WcArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WcArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WcArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
